
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY FSMcafetera_tb IS
END FSMcafetera_tb;
 
ARCHITECTURE behavior OF FSMcafetera_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FSMcafetera
    PORT(
         clk : IN  std_logic;
         BT : IN  std_logic_vector(3 downto 0);
         dispA : OUT  std_logic_vector(3 downto 0);
         dispB : OUT  std_logic_vector(3 downto 0);
         dispC : OUT  std_logic_vector(3 downto 0);
         dispD : OUT  std_logic_vector(3 downto 0);
         led : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal BT : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal dispA : std_logic_vector(3 downto 0);
   signal dispB : std_logic_vector(3 downto 0);
   signal dispC : std_logic_vector(3 downto 0);
   signal dispD : std_logic_vector(3 downto 0);
   signal led : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FSMcafetera PORT MAP (
          clk => clk,
          BT => BT,
          dispA => dispA,
          dispB => dispB,
          dispC => dispC,
          dispD => dispD,
          led => led
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		BT<="0001";
		wait for 40 ns;
		BT<="0000";

      wait;
   end process;

END;
