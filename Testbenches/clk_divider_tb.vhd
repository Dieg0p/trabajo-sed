
--libreria
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------

--entidad
entity clk_divider_tb is
end clk_divider_tb;
------------------------------------------------------------------
--arquitectura
architecture behavior of clk_divider_tb is

--declaracion de componentes a examinar
component clk_divider is
	port(	clk,reset: in std_logic;
			p_t: out std_logic);
end component;

--señales que vamos a generar: inputs:
signal clk: std_logic :='0';
signal reset: std_logic :='0';
--señales que vamos a comprobar si estan bien: outputs:
signal p_t: std_logic;

begin
--instanciacion:
uut: clk_divider port map(
	clk=> clk,
	reset=> reset,
	P_t=> P_t);


P_RESET: process
			begin
				reset<='0';
				wait for 33 ns;
				reset<='1';
				wait for 89 ns;
				reset<='0';
				assert false report "puesto el reset" severity note;
				wait;-- espera para siempre
			end process;

P_Clk: Process
			begin
				clk <= '1';
				wait for 10 ns;
				clk <= '0';
				wait for 10 ns;
			end process;

end behavior;

