
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY display_refresh_tb IS
END display_refresh_tb;
 
ARCHITECTURE behavior OF display_refresh_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT display_refresh
    PORT(
         dispA : IN  std_logic_vector(3 downto 0);
         dispB : IN  std_logic_vector(3 downto 0);
         dispC : IN  std_logic_vector(3 downto 0);
         dispD : IN  std_logic_vector(3 downto 0);
         clk : IN  std_logic;
         segment : OUT  std_logic_vector(7 downto 0);
         digctrl : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal dispA : std_logic_vector(3 downto 0) := (others => '0');
   signal dispB : std_logic_vector(3 downto 0) := (others => '0');
   signal dispC : std_logic_vector(3 downto 0) := (others => '0');
   signal dispD : std_logic_vector(3 downto 0) := (others => '0');
   signal clk : std_logic := '0';

 	--Outputs
   signal segment : std_logic_vector(7 downto 0);
   signal digctrl : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: display_refresh PORT MAP (
          dispA => dispA,
          dispB => dispB,
          dispC => dispC,
          dispD => dispD,
          clk => clk,
          segment => segment,
          digctrl => digctrl
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		

      -- insert stimulus here 
		wait for 40 ns;
		dispA<="0100";
		dispB<="1001";
		dispC<="0000";
		dispD<="1111";

      wait;
   end process;

END;
