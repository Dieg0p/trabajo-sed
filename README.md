# README #

DISEÑO DE UNA CAFETERA EN VHDL
Repositorio del trabajo de la asignatura " Sistemas Electrónicos Digitales". 

![top.JPG](https://bitbucket.org/repo/LEnqMo/images/4124557071-top.JPG)
### Integrantes ###

* Marta Rodríguez Simal
* Sergio Romero Ortega
* Diego Pelayo Martínez

### Explicación del trabajo ###

* Enunciado:
*Diseñe el control de una cafetera automática. La cafetera contiene dos botones programables (“corto” y “largo”) para preparar cafés cortos o largos, además de un botón de                           encendido (“encendido”). El control de la cafetera utilizará estos botones como entradas y generará una salida “bomba” que al activarse pondrá en funcionamiento la bomba de presión. Por defecto, la programación de los botones programables viene predefinida como 10 segundos y 20 segundos para “corto” y “largo” respectivamente.

* Funcionalidades añadidas:
Como mejoras de diseño al enunciado propuesto hemos añadido:
	- Introducción de monedas para el pago del café.
	- Selección del nivel de azúcar
	- Dieferntes tipos de café. Solo, con leche, con nata o manchado.
	- Impresión por pantalla de la situación de la cafetera mediante el uso del display y los leds.


### Detalles ###

* V 2.0 

### Instrucciones ###

Una vez encendida la cafetera se quedará en reposo hasta que se accione cualquier pulsador. Una vez suceda ese evento tendremos que introducir monedas hasta llegar a 1€, nuestra cafetera admite monedas de 10, 20 y 50 céntimos así como de 1€.

Si no introduce el importe en un tiempo prudencial o no se abona el importe total del café, la máquina le devolverá el dinero quedando a la espera de un nuevo pedido.
Si  has introducido un 1€ satisfactoriamente la maquina le pedirá cuanto azúcar quiere en su café. Por defecto se sirve un nivel medio de azúcar que puede aumentar o disminuir al gusto.

Después puede seleccionar un café corto o largo y añadirle leche, nata, hacer un cortado o tomarlo solo. Por defecto se sirve un café solo.

La cafetera mostrará una cuenta atrás para indicarle cuanto le queda a la máquina para terminar. Además los leds se iluminaran tras pasar una etapa para que pueda conocer si se encuentra  eligiendo el nivel de azúcar, el tipo de café o algún elemento añadido.



### ¡OJO! ###

Para montar el proyecto:
- Nos vamos a "generar tcl", pinchamos en project y le damos a "generate tcl" -> "modifies properties". Nos pondrá al día el archivo .tcl.
- Después nos vamos a "view-> panels -> tcl console".
- Le damos a nuestro fichero y reconstruimos el proyecto "rebuild_project"
- De esa forma tenemos el proyecto preparado para el ISE.

NOTA IMPORTANTE: Solo subir al repositorio los .vhd (.ucf y .tcl ya los subimos al final para evitar tener archivos inútiles aquí)