
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity input_filter is
	generic(n_bit: integer range 0 to 2**5-1 := 8);
	port(	b_in: in std_logic_vector(n_bit - 1 downto 0);
			clk: in std_logic;
			b_out: out std_logic_vector(n_bit - 1 downto 0)
	);
end ipnut_filter;

architecture Behavioral of input_filter is
component antirrebote
	port(
			btn: in std_logic;
			clk_T: in std_logic;
			reset: in std_logic;
			sin_reb: out std_logic);
end component;
component flanco
	port(
			bt: in std_logic;
			clk: in std_logic;
			pulso: out std_logic);
end component;
component clk_divider
	generic(max: integer range 0 to 2**26-1 := 150);
	port(	clk, reset: in std_logic;
			p_t: out std_logic);
end component;

for all: antirrebote use entity work.antirrebote(behavioral);
for all: flanco use entity work.flanco(comportamental);
for all: clk_divider use entity work.clk_divider(comportamental);

signal pulso: std_logic;
signal aux: std_logic_vector (b_in'range);
begin
	I_pulso: clk_divider
		generic map (max=> 500000) --500000 para sintesis --3 para simulacion 
		port map(
			clk => clk,
			reset => '0',
			p_t => pulso);
	fil: for i IN b_in'range GENERATE
	I_antirrebote: antirrebote port map(
			Btn => b_in(i),
			clk_T=>pulso,
			reset=> '0',
			sin_reb => aux(i));
	I_detec: flanco port map(
			bt => aux(i),
			clk => clk,
			pulso => b_out(i));
	end GENERATE;

end Behavioral;

