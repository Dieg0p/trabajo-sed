
--Librerias

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
----------------------------------------------------------
-- entidad

entity dec4to8 is 

	port (	bin: in std_logic_vector (3 downto 0);
				seg: out std_logic_vector (7 downto 0));

end dec4to8;
---------------------------------------------------------
--arquitectura comportamental abstracta

architecture abstracta of dec4to8 is
begin
	process (bin)
		begin
		if bin = "0000" then seg<="00000011";   -- 0
		elsif bin = "0001" then seg<="10011111";  --1
		elsif bin = "0010" then seg<="00100101";  --2
		elsif bin = "0011" then seg<="00001101";  --3
		elsif bin = "0100" then seg<="10011001";  --4
		elsif bin = "0101" then seg<="01001001";  --5
		elsif bin = "0110" then seg<="01000001";  --6
		elsif bin = "0111" then seg<="00011111";  --7
		elsif bin = "1000" then seg<="00000001";  --8
		elsif bin = "1001" then seg<="00011001";  --9
		elsif bin = "1010" then seg<="00000010";  --0.
		elsif bin = "1011" then seg<="10011110";  --1.
		elsif bin = "1100" then seg<="11111101";  -- -
		elsif bin = "1101" then seg<="01100001";  --E
		elsif bin = "1110" then seg<="11100011";  --L
		else  seg<="11111111"; --
		end if;
	end process;
end abstracta;
--------------------------------------------------------


