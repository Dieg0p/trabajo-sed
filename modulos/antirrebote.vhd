
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-----------------------------------------------------------------

--entidad
entity antirrebote is
    Port(	Btn : in std_logic;
		clk_T : in std_logic;
		reset : in std_logic;
		sin_reb : out std_logic -- señal sin rebotes
		  );
end antirrebote;

architecture behavioral of antirrebote is

signal aux: std_logic :='0';

begin

sin_reb <= aux;

process (clk_T,Btn,reset)
begin
	if reset = '1' then aux <= '0';
	elsif clk_T = '1' and clk_T'event then 
		aux <= Btn;
	end if;
end process;

end behavioral;
