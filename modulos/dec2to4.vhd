
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

----------------------------------------------------------
--entidad

entity dec2to4 is

	generic(logica: bit := '0');
	
	port (	I: in std_logic_vector(1 downto 0);
				E: in std_logic;
				S: out std_logic_vector (3 downto 0));

end dec2to4;
---------------------------------------------------------
--arquitectura comportamental abstracta

architecture abstracta of dec2to4 is
begin
	process(I,E)
	variable negE: std_logic;
	variable prevS: std_logic_vector (S'range);
	begin
		negE := not (E);
		if negE = '0' then prevS:= "0000";   
			elsif I="00" then prevS:="0001";  
			elsif I="01" then prevS:="0010"; 
			elsif I="10" then prevS:="0100"; 
			else prevS:="1000";
		end if;
		if logica='0' then S<=not (prevS);  --seleccionamos logica del disp.
			else S<= prevS;
		end if;
	end process;
end abstracta;
----------------------------------------------------------


