----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cafe_top is
    Port ( clk : in  STD_LOGIC;
           Botones : in  STD_LOGIC_VECTOR (3 downto 0);
           display_number : out  STD_LOGIC_VECTOR (7 downto 0);
           display_ctrl : out  STD_LOGIC_VECTOR (3 downto 0);
           led : out  STD_LOGIC_VECTOR (7 downto 0));
end cafe_top;

architecture Behavioral of cafe_top is

	component input_filter is
		generic(n_bit: integer range 0 to 2**5-1 := 8);
		port(	b_in: in std_logic_vector(n_bit - 1 downto 0);
				clk: in std_logic;
				b_out: out std_logic_vector(n_bit - 1 downto 0)
		);
	end component;

	COMPONENT FSMcafetera
	PORT(
		clk : IN std_logic;
		BT : IN std_logic_vector(3 downto 0);          
		dispA : OUT std_logic_vector(3 downto 0);
		dispB : OUT std_logic_vector(3 downto 0);
		dispC : OUT std_logic_vector(3 downto 0);
		dispD : OUT std_logic_vector(3 downto 0);
		led : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	COMPONENT display_refresh
	PORT(
		dispA : IN std_logic_vector(3 downto 0);
		dispB : IN std_logic_vector(3 downto 0);
		dispC : IN std_logic_vector(3 downto 0);
		dispD : IN std_logic_vector(3 downto 0);
		clk : IN std_logic;          
		segment : OUT std_logic_vector(7 downto 0);
		digctrl : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;

signal b: std_logic_vector(3 downto 0);
signal dispA,dispB,dispC,dispD: std_logic_vector(3 downto 0);

begin

	Inst_input_filter: input_filter
		generic map(n_bit=>4)
		port map(
			b_in => botones,
			clk => clk,
			b_out =>b 
	);

	Inst_FSMcafetera: FSMcafetera PORT MAP(
		clk => clk,
		BT => b,
		dispA => dispA,
		dispB => dispB,
		dispC => dispC,
		dispD => dispD,
		led => led
	);

	Inst_display_refresh: display_refresh PORT MAP(
		dispA => dispA,
		dispB => dispB,
		dispC => dispC,
		dispD => dispD,
		clk => clk,
		segment => display_number,
		digctrl => display_ctrl
	);


end Behavioral;

