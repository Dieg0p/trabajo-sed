
--librerias
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

------------------------------------------------------------------

--entidad
entity clk_divider is
	generic (max: integer range 0 to 2**26-1 := 10);
	port(	clk,reset: in std_logic;
			p_t: out std_logic);
end clk_divider;

------------------------------------------------------------------


--arquitectura comporatametal serie divisor de frecuencia
architecture comportamental of clk_divider is
signal cuenta: natural range 0 to 2**26-1 := 0; --registro para contar
					-- 2^26 > 50 000 000
begin

P_contar:	process(clk,reset)
			begin
				if reset = '1' then
					cuenta<=0;
					p_t<='0';
				elsif clk'event and clk='1' then
					if cuenta >= max - 1 then
						p_t<='1';
						cuenta<=0;
					else
						p_t<='0';
						cuenta<=cuenta + 1;
					end if;
				end if;
			end process;
end comportamental;
-------------------------------------------------------------------


