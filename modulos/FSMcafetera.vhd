
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FSMcafetera is
	generic(	t1: integer range 0 to 2**8 - 1 := 10; -- tiempo de apagado cuando no hay actividad (en segundos)
				t2: integer range 0 to 2**8 - 1 := 5;-- tiempo de espera para el nivel de azucar
				t3: integer range 0 to 2**8 - 1 := 20;-- tiempo de devolucion del dinero si no se selecciona bebida
				t4: integer range 0 to 2**8 - 1 := 10;-- tiempo cafe corto
				t5: integer range 0 to 2**8 - 1 := 20;-- tiempo cafe largo
				t6: integer range 0 to 2**8 - 1 := 10;-- tiempo de estado inicial si no se selecciona complemento
				t7: integer range 0 to 2**8 - 1 := 8;--tiempo de complemento leche
				t8: integer range 0 to 2**8 - 1 := 4;-- tiempo de devolucion de monedas
				t9: integer range 0 to 2**8 - 1 := 25;-- tiempo de devolucion del dinero si no se insertan más monedas
				t10: integer range 0 to 2**8 - 1 := 4;--tiempo de complemento cortado
				t11: integer range 0 to 2**8 - 1 := 7);--tiempo de complemento nata
	port(	BT: in std_logic_vector(3 downto 0);
			clk: in std_logic;
			dispA, dispB, dispC, dispD: out std_logic_vector(3 downto 0);
			led: out std_logic_vector(7 downto 0));
end FSMcafetera;

architecture Behavioral of FSMcafetera is

signal acu: integer range 0 to 2**8-1 :=0;
type estados is (est0, est1, est2, est3, est4, est5, est6, est7);
signal presente, futuro: estados:=est0;
constant segundo: integer:= 50000000; -- para simulacion 2 para sintetizar 50 000 000
signal contador: integer range 0 to 2**26 - 1 := 0;
signal tiempo: integer range 0 to 50 := 0; -- en segundos
signal tiempo_inverso: integer range 0 to 50 :=0;


signal nivel_azucar: integer range 0 to 4 :=2;--"0" minimo, "4" maximo
type cafe is (corto, largo);
signal tipo_cafe: cafe:=corto;
type complemento is (sin, nata, cortado, leche);
signal tipo_complemento: complemento:=sin;

begin		
clocked: process(clk)
			begin
			if (clk'event and clk='1') then
				presente <= futuro;
			end if;
			end process;
			

future:	process(clk)
		begin
			if clk='1' and clk'event then
			case presente is
				when est0 =>
						 --empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- cuando se aprieta cualquier boton se enciende la máquina
						if BT/="0000" then futuro<=est1;
							contador<=0; tiempo<=0;
						end if;
				when est1 => 
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- si no hay monedas insertadas se va al estado stand-by
						if acu=0 then
							if BT="0001" then acu<= acu+10;
							elsif BT="0010" then acu<=acu+20;
							elsif BT="0100" then acu<=acu+50;
							elsif BT="1000" then acu<=acu+100;
							elsif tiempo>=t1 then 
								futuro<=est0; contador<=0;tiempo<=0;
							end if;
						-- si hay monedas insertadas
						elsif acu<100 then
							-- si se insertan monedes se suma la cantidad y se resetea el tiempo
							if BT="0001" then 
										acu<= acu+10;
										tiempo<=0;
										contador<=0;
							elsif BT="0010" then 
										acu<=acu+20;
										tiempo<=0;
										contador<=0;
							elsif BT="0100" then 
										acu<=acu+50;
										tiempo<=0;
										contador<=0;
							elsif BT="1000" then 
										acu<=acu+100;
										tiempo<=0;
										contador<=0;
							-- si no se insertan más monedas, se va al estado de devolución
							elsif tiempo >= t9 then
								futuro<=est7; tiempo<=0; contador<=0; acu<=0;
							end if;
							-- si ya se ha completa la cantidad se pasa al estado de nivel de azucar
						elsif acu=100 then futuro<=est2; acu<=0;
							-- si se pasa de la cantidad, se pasa al estado de devolucion
						elsif acu>100 then futuro<=est7; acu<=0;
						end if;
						
				when est2 => 
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para el nivel de azucar
						if BT="0001" then nivel_azucar<= nivel_azucar +1;
						elsif BT="0010" then nivel_azucar<= nivel_azucar -1;
						elsif tiempo >= t2 then
								futuro<=est3; contador<=0; tiempo<=0; nivel_azucar<=2;
						end if;
						
				when est3 => 
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para el tiepo de cafe
						if BT="0001" then tipo_cafe<= corto;
								futuro<=est4; contador<=0; tiempo<=0;
						elsif BT="0010" then tipo_cafe<= largo;
								futuro<=est4; contador<=0; tiempo<=0;
						elsif tiempo >= t3 then
								futuro<=est7; contador<=0; tiempo<=0;
						end if;
						
				when est4 => 
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- espera el tiempo de activacion de la bomba
						if tipo_cafe=corto then
								tiempo_inverso<= t4 - tiempo;
						elsif tipo_cafe=largo then
								tiempo_inverso<= t5 -tiempo;
						end if;
						if tipo_cafe=corto and tiempo >= t4 then
								futuro<=est5; contador<=0; tiempo<=0;
						elsif tipo_cafe=largo and tiempo >= t5 then
								futuro<=est5; contador<=0; tiempo<=0;
						end if;
						
				when est5 => 
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- pulsar para elegir el complemento
						if BT="0001" then tipo_complemento<= leche;
								futuro<=est6; contador<=0; tiempo<=0;
						elsif BT="0010" then tipo_complemento<= cortado;
								futuro<=est6; contador<=0; tiempo<=0;
						elsif BT="0100" then tipo_complemento<= nata;
								futuro<=est6; contador<=0; tiempo<=0;
						elsif BT="1000" then tipo_complemento<= sin;
								futuro<=est6; contador<=0; tiempo<=0;
						elsif tiempo >= t6 then
								futuro<=est1; contador<=0; tiempo<=0;
						end if;
						
				when est6 => 
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- espera el tiempo del servicio del complemento
						if tipo_complemento=leche then tiempo_inverso<= t7 - tiempo;
						elsif tipo_complemento=cortado then tiempo_inverso<= t10 - tiempo;
						elsif tipo_complemento=nata then tiempo_inverso<= t11 - tiempo;
						end if;
						
						if tipo_complemento=leche and tiempo >= t7 then
								futuro<=est1; contador<=0; tiempo<=0;
						elsif tipo_complemento=cortado and tiempo >= t10 then
								futuro<=est1; contador<=0; tiempo<=0;
						elsif tipo_complemento=nata and tiempo >= t11 then
								futuro<=est1; contador<=0; tiempo<=0;
						elsif tipo_complemento=sin then
								futuro<=est1; contador<=0; tiempo<=0;
						end if;
						
				when est7 =>
						-- empieza a contar el tiempo
							contador<= contador+1;
							if contador >= segundo then 
								tiempo<=tiempo + 1;
								contador<=0;
							end if;
						-- si se aprieta cualquier boton se va al estado de inserción de monedas
						if BT/="0000" then 
								futuro <= est1; tiempo<=0; contador<=0;
						end if;
						-- pasado un tiempo si no se aprieta se vuelve al estado de inserción
						if tiempo >= t8 then
								futuro<=est1; tiempo<=0; contador<=0;
						end if;
			end case;
			end if;
		end process;
	
salida: process(clk)
	begin
		if clk'event and clk='1' then
		case presente is
			when est1 =>
							led<="00000000";
							dispA<= "1111";
							dispB<= "1010";
							case acu is
								when 10 | 15 => dispC<= "0001";
								when 20 | 25 => dispC<= "0010";
								when 30 | 35 => dispC<= "0011";
								when 40 | 45 => dispC<= "0100";
								when 50 | 55 => dispC<= "0101";
								when 60 | 65 => dispC<= "0110";
								when 70 | 75 => dispC<= "0111";
								when 80 | 85 => dispC<= "1000";
								when 90 | 95 => dispC<= "1001";
								when others => dispC<= "0000";
							end case;
							case acu is
								when 5|15|25|35|45|55|65|75|85|95 =>
										dispD<= "0101";
								when others => dispD <= "0000";
							end case;
			when est2 =>
							led<="00000001";
							case nivel_azucar is
								when 1 => 
											dispA<= "1100";
											dispB<= "1111";
											dispC<= "1111";
											dispD<= "1111";
								when 2 => 
											dispA<= "1100";
											dispB<= "1100";
											dispC<= "1111";
											dispD<= "1111";
								when 3 => 
											dispA<= "1100";
											dispB<= "1100";
											dispC<= "1100";
											dispD<= "1111";
								when 4 => 
											dispA<= "1100";
											dispB<= "1100";
											dispC<= "1100";
											dispD<= "1100";
								when others => 
											dispA<= "1111";
											dispB<= "1111";
											dispC<= "1111";
											dispD<= "1111";
							end case;
							
			when est3 =>
						led<="00000011";
							-- SEL1 --
							dispA<= "0101";
							dispB<= "1101";
							dispC<= "1110";
							dispD<= "0001";
							
			when est4 => 
							led<="10000011";
							case tipo_cafe is
								when corto => dispA<="0001";
								when others => dispA<="0010";
							end case;
							
							dispB<="1111";
							
							
							case tiempo_inverso is
								when 0 to 9 =>dispC<="0000";
								when 10 to 19 =>dispC<="0001";
								when 20=> dispC<="0010";
								when others => dispC<= "1111";
							end case;
							
							case tiempo_inverso is
								when 0 | 10 | 20 =>dispD<= "0000";
								when 1 | 11 =>dispD<= "0001";
								when 2 | 12 =>dispD<= "0010";
								when 3 | 13 =>dispD<= "0011";
								when 4 | 14 =>dispD<= "0100";
								when 5 | 15 =>dispD<= "0101";
								when 6 | 16 =>dispD<= "0110";
								when 7 | 17 =>dispD<= "0111";
								when 8 | 18 =>dispD<= "1000";
								when 9 | 19 =>dispD<= "1001";
								when others =>dispD<= "1111";
							end case;

			when est5 =>
							led<="00000111";
							-- SEL1 --
							dispA<= "0101";
							dispB<= "1101";
							dispC<= "1110";
							dispD<= "0010";
							
			when est6 => 
							led<="00000111";
							case tipo_complemento is
								when leche => dispA<="0001";
								when cortado => dispA<="0010";
								when nata => dispA<="0011";
								when sin => dispA<="0100";
							end case;
							
							dispB<="1111";
							
							case tiempo_inverso is
								when 0 to 9 =>dispC<="0000";
								when 10 to 19 =>dispC<="0001";
								when 20=> dispC<="0010";
								when others => dispC<= "1111";
							end case;
							
							case tiempo_inverso is
								when 0 | 10 | 20 =>dispD<= "0000";
								when 1 | 11 =>dispD<= "0001";
								when 2 | 12 =>dispD<= "0010";
								when 3 | 13 =>dispD<= "0011";
								when 4 | 14 =>dispD<= "0100";
								when 5 | 15 =>dispD<= "0101";
								when 6 | 16 =>dispD<= "0110";
								when 7 | 17 =>dispD<= "0111";
								when 8 | 18 =>dispD<= "1000";
								when 9 | 19 =>dispD<= "1001";
								when others =>dispD<= "1111";
							end case;
			when est7 =>
							led<="00000000";		
							-- SEL1 --
							dispA<= "1101";
							dispB<= "1101";
							dispC<= "1101";
							dispD<= "1101";
			when others =>
							led<="00000000";
							-- SEL1 --
							dispA<= "1111";
							dispB<= "1111";
							dispC<= "1111";
							dispD<= "1111";
		end case;
		end if;
	end process;
	
		
end Behavioral;
