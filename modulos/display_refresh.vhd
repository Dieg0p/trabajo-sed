
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity display_refresh is
	port(	dispA,dispB,dispC,dispD: in std_logic_vector(3 downto 0);
			clk: in std_logic;
			segment: out std_logic_vector(7 downto 0);
			digctrl: out std_logic_vector(3 downto 0));
end display_refresh;

architecture Behavioral of display_refresh is

	component mux4to1
	generic(nbits: natural range 1 to 32:=4);
	port(	inA: in std_logic_vector (nbits - 1 downto 0);
			inB: in std_logic_vector (nbits - 1 downto 0);
			inC: in std_logic_vector (nbits - 1 downto 0);
			inD: in std_logic_vector (nbits - 1 downto 0);
			selecS:	in	std_logic_vector(1 downto 0);
			outZ: out std_logic_vector (nbits -1 downto 0));
	end component;
	COMPONENT dec4to8
	PORT(
		bin : IN std_logic_vector(3 downto 0);          
		seg : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
	COMPONENT dec2to4
	PORT(
		I : IN std_logic_vector(1 downto 0);
		E : IN std_logic;          
		S : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	COMPONENT balance4
	PORT(
		clk_T : IN std_logic;          
		balance : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;
	COMPONENT clk_divider
		generic (max: integer range 0 to 2**26-1 := 10);
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;          
		p_t : OUT std_logic
		);
	END COMPONENT;
		
for all: mux4to1 use entity work.mux4to1(abstracta);
for all: dec2to4 use entity work.dec2to4(abstracta);
for all: clk_divider use entity work.clk_divider(comportamental);
for all: balance4 use entity work.balance4(behav);
for all: dec4to8 use entity work.dec4to8(abstracta);	
	

signal a_i: std_logic_vector(dispA'range);
signal b_i: std_logic_vector(1 downto 0);
signal c_i: std_logic;
begin

Inst_mux4to1: mux4to1 PORT MAP(
		inA => dispA,
		inB => dispB,
		inC => dispC,
		inD => dispD,
		selecS => b_i,
		outZ => a_i
	); 

Inst_dec2to4: dec2to4 PORT MAP(
		I => b_i,
		E => '0',
		S => digctrl
	);
	
Inst_dec4to8: dec4to8 PORT MAP(
		bin => a_i,
		seg => segment
	);

Inst_balance4: balance4 PORT MAP(
		clk_T => c_i,
		balance => b_i
	);
	
Inst_clk_divider: clk_divider 
	generic map( max => 50000) --para sintetizar 50000 --para simulacion 2
	PORT MAP(
		clk => clk,
		reset => '0',
		p_t => c_i
	);


end Behavioral;

