

--librerias
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-------------------------------------------------------------------

--entidad
entity balance4 is
	port(	clk_T: in std_logic;
			balance: out std_logic_vector(1 downto 0):="00");
end balance4;
-------------------------------------------------------------------

--arquitectura
architecture behav of balance4 is
signal cuenta: natural range 0 to 3 :=0;
begin 
	process(clk_T)
	begin
		if clk_T = '1' and clk_T'event then
			if cuenta = 0 then balance<="00";
				cuenta<=cuenta +1 ;
			elsif cuenta = 1 then 
				balance<="01";
				cuenta<=cuenta +1;
			elsif cuenta = 2 then
				balance<="10";
				cuenta<=cuenta +1;
			else
				balance<="11";
				cuenta<=0;
			end if;
		end if;
	end process;
end behav;
------------------------------------------------------------------
		
