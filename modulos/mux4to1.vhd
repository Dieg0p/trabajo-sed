--Librerias

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-----------------------------------------------------------
--Entidad

entity  mux4to1 is
	generic(nbits: natural range 1 to 32 :=4);

	port(	inA: in std_logic_vector (nbits - 1 downto 0);
			inB: in std_logic_vector (nbits - 1 downto 0);
			inC: in std_logic_vector (nbits - 1 downto 0);
			inD: in std_logic_vector (nbits - 1 downto 0);
			selecS: in std_logic_vector(1 downto 0);
			outZ: out std_logic_vector (nbits - 1 downto 0));

end mux4to1;
-----------------------------------------------------------
--Arquitectura comportamental abstracta

architecture abstracta of mux4to1 is

	begin
		process(inA,inB,inC,inD,selecS)
		begin
			case selecS IS
				when "00" => outZ <= inA;
				when "01" => outZ <= inB;
				when "10" => outZ <= inC;
				when others => outZ <= inD;
			end case;
		end process;

end abstracta;
-----------------------------------------------------------

