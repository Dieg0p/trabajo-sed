--librerias
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--------------------------------------------------------------------
-- entidad
entity flanco is

	port(	BT,clk: in std_logic;
			pulso: out std_logic:='0');
end flanco;
--------------------------------------------------------------------
-- arquitectura comportamental serie
architecture comportamental of flanco is
signal BT_R1,BT_R2: std_logic;  --son las salidas de los registros
begin

reg_D1:	process(clk,BT)  --proceso para el primer biestable tipo D
		begin
			if (clk'event and clk='1') then
				BT_R1<=BT;
			end if;
		end process;

reg_D2: process(clk,BT_R1)
		begin
			if(clk'event and clk='1') then
				BT_R2<=BT_R1;
			end if;
		end process;
pulso<=BT_R1 and (not BT_R2); -- es la parte combinacional del detector

end comportamental;
--------------------------------------------------------------------

